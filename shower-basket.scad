// clang-format off
include <utils/utils.scad>;
// clang-format on

/* [Rail] */
rail_width = 45;
rail_depth_total = 22.5;
rail_front_depth = 10;
// It's actually 90cm but you get the idea...
rail_length = 200;
rail_slot_width = 18.5;
rail_slot_depth = 11;
// I forgot to measure this...
rail_lower_cap_height = 30;
rail_back_width = 21.8;
rail_back_bracket_height = 55;
rail_back_bracket_thickness = 3;
rail_back_clearance = rail_back_bracket_thickness;
// I didn't measure this...
rail_bottom_spike_length = 3;
rail_bottom_spike_thickness = 2;
rail_bottom_spike_height = 4;

/* [Bracket] */
bracket_width = 120;
bracket_height = 30;
bracket_thickness = 3;
bracket_clearance = 0.5;

/* [Positioner]  */
positioner = true;
positioner_length = 100;
positioner_notch_width = 5;
positioner_notch_length = 5;

/* [Basket] */
basket = true;
basket_width = 150;
basket_depth = 100;
basket_height = 100;
basket_thickness = 3;
basket_hole_size = 15;
basket_hole_distance = 20;
basket_hole_shape = "square"; // ["square","circle"]

/* [Wall] */
wall = true;
wall_thickness = 10;

/* [Precision] */
$fa = $preview ? 2 : 0.5;
$fs = $preview ? 2 : 0.5;
epsilon = 0.5;

module
basket()
{
  render() difference()
  {
    // base shape
    cube([ basket_width, basket_depth, basket_height ], center = true);
    // container
    translate([ 0, 0, basket_thickness / 2 + epsilon ]) cube(
      [
        basket_width - 2 * basket_thickness,
        basket_depth - 2 * basket_thickness,
        basket_height - basket_thickness +
        epsilon
      ],
      center = true);
    n_holes_x = floor((basket_width - 2 * basket_thickness - basket_hole_size) /
                      basket_hole_distance);
    n_holes_y = floor((basket_depth - 2 * basket_thickness - basket_hole_size) /
                      basket_hole_distance);
    n_holes_z =
      floor((basket_height - 2 * basket_thickness - basket_hole_size) /
            basket_hole_distance);
    trans_bottom = [for (ix = [-
                           n_holes_x:
                                   2:n_holes_x]) for (iy =
                                                        [-n_holes_y:
                                                                  2:n_holes_y])
        [basket_height + epsilon,
         [ 0, 0, 0 ],
         [ ix / 2 * basket_hole_distance, iy / 2 * basket_hole_distance, 0 ]]];
    trans_front = [for (ix =
                          [-n_holes_x:2:n_holes_x]) for (iz = [-
                                                           n_holes_z:
                                                                   2:n_holes_z])
        [basket_depth + epsilon,
         [ 90, 0, 0 ],
         [ ix / 2 * basket_hole_distance, 0, iz / 2 * basket_hole_distance ]]];
    trans_sides = [for (iy =
                          [-n_holes_y:2:n_holes_y]) for (iz = [-
                                                           n_holes_z:
                                                                   2:n_holes_z])
        [basket_width + epsilon,
         [ 0, 90, 0 ],
         [ 0, iy / 2 * basket_hole_distance, iz / 2 * basket_hole_distance ]]];
    // bottom holes
    for (size_rot_trans = concat(trans_bottom, trans_front, trans_sides)) {
      translate(size_rot_trans[2])
        rotate(size_rot_trans[1]) if (basket_hole_shape == "circle")
      {
        cylinder(d = basket_hole_size, h = size_rot_trans[0], center = true);
      }
      else if (basket_hole_shape == "square")
      {
        cube([ basket_hole_size, basket_hole_size, size_rot_trans[0] ],
             center = true);
      }
    }
  }
  // hooks
  for (i = [ -1, 1 ]) {
    hook_width =
      (bracket_width - rail_width - 2 * bracket_thickness) / 2 * 0.75;
    hook_depth = bracket_thickness + 2 * bracket_clearance;
    translate([
      i *
        (bracket_width / 2 + (bracket_width / 2 - rail_width / 2 +
                              bracket_thickness + 2 * bracket_clearance) /
                               2) /
        2,
      basket_depth / 2 + hook_depth / 2,
      basket_height / 2 - basket_thickness / 2
    ])
    {
      cube([ hook_width, hook_depth, basket_thickness ], center = true);
      translate([
        0,
        hook_depth / 2 + basket_thickness / 2,
        -bracket_height / 2 + basket_thickness / 2
      ]) cube([ hook_width, basket_thickness, bracket_height ], center = true);
    }
  }
}

module rail(rail_length = rail_length, spikes = true, back_bracket = true)
{
  render() difference()
  {
    union()
    {
      // front
      translate([ -rail_width / 2, 0, 0 ])
        cube([ rail_width, rail_front_depth, rail_length ]);
      // center back
      translate([ -rail_back_width / 2, rail_front_depth, 0 ]) cube([
        rail_back_width,
        rail_depth_total - rail_front_depth - rail_back_bracket_thickness,
        rail_length
      ]);
      if (back_bracket) {
        // back bracket
        translate([
          -rail_back_width / 2,
          rail_depth_total - rail_back_bracket_thickness,
          0
        ])
          cube([
            rail_back_width,
            rail_back_bracket_thickness,
            rail_back_bracket_height
          ]);
      }
    }
    translate([ -rail_slot_width / 2, -epsilon, -epsilon / 2 ]) cube(
      [ rail_slot_width, rail_slot_depth + epsilon, rail_length + epsilon ]);
  }
  if (spikes) {
    // spikes
    for (i = [ -1, 1 ]) {
      translate([
        i * (rail_slot_width / 2 - rail_bottom_spike_length / 2),
        rail_bottom_spike_thickness / 2,
        rail_bottom_spike_height / 2
      ])
        cube(
          [
            rail_bottom_spike_length,
            rail_bottom_spike_thickness,
            rail_bottom_spike_height
          ],
          center = true);
    }
  }
}

// the shower head rail
if ($preview) {
  color("lightgray") rail(rail_length);
}

// the wall
if (wall && $preview) {
  translate([
    0,
    wall_thickness / 2 + rail_depth_total,
    rail_length * 1.5 / 2 - rail_length * 0.5 / 2
  ]) color("#ffcf6b", 0.5)
    cube(
      [
        max(bracket_width, basket_width) * 1.5,
        wall_thickness,
        rail_length * 1.5
      ],
      center = true);
}

// the bracket
module
bracket_side()
{
  render() difference()
  {
    translate([ -rail_slot_width / 2, -bracket_thickness ])
    {
      // front
      translate([ rail_slot_width, -bracket_clearance, 0 ]) cube([
        (bracket_width - rail_slot_width) / 2 - bracket_clearance,
        bracket_thickness,
        bracket_height
      ]);
      // front slot fill
      translate([ bracket_clearance, -bracket_clearance, 0 ]) cube([
        rail_slot_width - bracket_clearance,
        bracket_thickness,
        bracket_height / 2 - bracket_clearance / 2
      ]);
      // holder bottom
      translate([ bracket_clearance, bracket_thickness - bracket_clearance, 0 ])
        cube([
          rail_slot_width / 2 - 1.5 * bracket_clearance,
          rail_slot_depth,
          bracket_height / 2
        ]);
      // holder top
      translate([ bracket_clearance, bracket_thickness, bracket_height / 2 ])
        cube([
          rail_slot_width / 2 - 1.5 * bracket_clearance,
          rail_slot_depth - bracket_clearance,
          bracket_height / 2
        ]);
      // side arms
      chained_hull()
      {
        translate(
          [ rail_slot_width, +bracket_thickness - bracket_clearance, 0 ])
          cube([
            (rail_width - rail_slot_width) / 2 + bracket_thickness +
              bracket_clearance,
            epsilon,
            bracket_height
          ]);
        translate([ rail_slot_width, rail_front_depth + bracket_thickness, 0 ])
          cube([
            (rail_width - rail_slot_width) / 2 + bracket_thickness +
              bracket_clearance,
            bracket_thickness,
            bracket_height
          ]);
        translate([
          rail_slot_width / 2 - rail_back_width / 2 + bracket_clearance,
          rail_depth_total,
          0
        ])
          cube([
            rail_back_width + bracket_thickness,
            bracket_thickness - bracket_clearance,
            bracket_height
          ]);
      }
    }
    // remove slots for the spikes at the bottom of the rail
    translate([ 0, 0, -epsilon / 2 ]) linear_extrude(bracket_height + epsilon)
      offset(bracket_clearance) projection()
        rail(spikes = true, back_bracket = false);
    // remove back slots
    translate([
      -rail_back_width / 2,
      rail_depth_total - bracket_thickness,
      bracket_height * 2 / 3 - epsilon / 2
    ])
      cube([
        rail_back_width / 2,
        rail_back_bracket_thickness,
        bracket_height / 3 +
        epsilon
      ]);
    translate([
      -bracket_clearance / 2,
      rail_depth_total - bracket_thickness,
      bracket_height * 1 / 3 - epsilon / 2
    ])
      cube([
        rail_back_width / 2 + bracket_clearance / 2,
        rail_back_bracket_thickness,
        bracket_height * 2 / 3 +
        epsilon
      ]);
    if (positioner) {
      // small notch for the heightener
      translate([
        -(positioner_notch_width + 2 * bracket_clearance) / 2,
        rail_depth_total - rail_back_bracket_thickness,
        -epsilon / 2
      ])
        cube([
          positioner_notch_width + 2 * bracket_clearance,
          rail_back_bracket_thickness,
          positioner_notch_length
        ]);
    }
  }
}

translate([
  0,
  0,
  rail_back_bracket_height + bracket_clearance +
    (positioner ? positioner_length + bracket_clearance : 0)
])
{
  // right side
  color("green", 0.5) bracket_side();

  // left side
  translate([ 0, 0, bracket_height ]) rotate([ 0, 180, 0 ]) color("red", 0.5)
    bracket_side();

  if (positioner) {
    color("lightblue") translate([
      -rail_back_width / 2,
      rail_depth_total - rail_back_bracket_thickness + bracket_clearance,
      -positioner_length -
      bracket_clearance
    ])
    {
      cube([
        rail_back_width,
        rail_back_bracket_thickness - 2 * bracket_clearance,
        positioner_length
      ]);
      translate([
        rail_back_width / 2 - positioner_notch_width / 2,
        0,
        positioner_length
      ])
        cube([
          positioner_notch_width,
          rail_back_bracket_thickness - 2 * bracket_clearance,
          positioner_notch_length
        ]);
      for (i = [ -1, 1 ]) {
        translate([
          i * (rail_back_width / 2 +
               (positioner_notch_width + bracket_clearance) / 2) +
            rail_back_width / 2 -
            (positioner_notch_width + bracket_clearance) / 2,
          0,
          0
        ])
        {
          cube([
            positioner_notch_width + bracket_clearance,
            rail_back_bracket_thickness - 2 * bracket_clearance,
            positioner_notch_width
          ]);
          // this i>=?... is dirty, I couldn't get the positioning right
          // otherwise...
          translate(
            [ i > 0 ? bracket_clearance : 0, 0, -positioner_notch_length ])
            cube([
              positioner_notch_width,
              rail_back_bracket_thickness - 2 * bracket_clearance,
              positioner_notch_width
            ]);
        }
      }
    }
  }
}

if (basket) {
  translate([
    0,
    -basket_depth / 2 - 2 * bracket_clearance - bracket_thickness,
    -basket_height / 2 + positioner_length + bracket_height +
      rail_back_bracket_height + 3 * bracket_clearance +
    basket_thickness
  ]) color("white", 0.3) basket();
}
